import React from 'react';
import logo from '../../img/logo.png'
import img from '../../img/01.png'
import './app.css';

function App () {
    return (<>
    <div className='container'>
        <header>
            <div className='logo'>
                <a href='https://www.youtube.com/watch?v=tMB4knXZNHM&ab_channel=Umperz0r'><img src={logo} alt='logo'></img></a>
            </div>
            <ul className='menu'>
                <li className='menu__item'>Functional Programming</li>
                <li className='menu__item'>Ramda</li>
                <li className='menu__item'>Fantasy Land Spec</li>
                <li className='menu__item'>Node.js for all</li>
            </ul>
        </header>
        <main>
            <div className='main-content'>
                <article className='content-menu'>
                    <ul className='second-menu'>
                        <li className='second-menu__item'>Ramda</li>
                        <li className='second-menu__item'>Why Ramda?</li>
                        <li className='second-menu__item'>What's Different?</li>
                        <li className='second-menu__item'>Introductions</li>
                        <li className='second-menu__item'>Philosophy</li>
                    </ul>
                </article>
                <article className='content-img'>
                    <img src={img} alt='img'></img>
                </article>
            </div>
            <div className='text-content'>
                <p className='text-content__item'>Lorem ipsum — классическая панграмма, условный, зачастую бессмысленный текст-заполнитель, вставляемый в макет страницы. Используется в качестве заполнителя по крайней мере с XVI века. Является искажённым отрывком из философского трактата Марка Туллия Цицерона «О пределах добра и зла», написанного в 45 году до н. э. на латинском языке, обнаружение сходства атрибутируется Ричарду МакКлинтоку.</p>
                <p className='text-content__item'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
        </main>
        <footer>
            <div className='footer'>
                <ul className='footer-menu'>
                    <li className='footer-menu__item'>Ramda</li>
                    <li className='footer-menu__item'>Why Ramda?</li>
                    <li className='footer-menu__item'>What's Different?</li>
                    <li className='footer-menu__item'>Introductions</li>
                    <li className='footer-menu__item'>Philosophy</li>
                </ul>
                <p className='footer-text'>Copyright © 2018</p>
            </div>
        </footer>
    </div>
    </>)
}

export default App